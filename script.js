// Завдання 1

function calculate(operand1, operand2, sign) {
  var result;

  if (isNaN(operand1) || isNaN(operand2)) {
    console.log("Помилка: внесені символи не цифри");
    return;
  }

  switch (sign) {
    case "+":
      result = operand1 + operand2;
      break;
    case "-":
      result = operand1 - operand2;
      break;
    case "*":
      result = operand1 * operand2;
      break;
    case "/":
      if (operand2 === 0) {
        console.log("Помилка: ділення на нуль не можливе");
        return;
      } else {
        result = operand1 / operand2;
      }
      break;
    default:
      console.log("Помилка: незрозумілий символ");
      return;
  }

  console.log("Результат: " + result);
}

//Завдання2
/*function power(base, exponent) {
  return Math.pow(base, exponent);
}

var base = parseFloat(prompt("Введите число"));
var exponent = parseFloat(prompt("Введите степень"));
var result = power(base, exponent);

alert(base + " в степени " + exponent + " = " + result);*/

//Завдання3

function playGame() {
  var choices = ["камінь", "ножиці", "бумага"];
  var computerChoice = Math.floor(Math.random() * 3); // Возвращает случайное целое число от 0 до 2
  var playerChoice = prompt("Оберіть: камінь, ножиці або бумага?");
  if (playerChoice !== "камінь" && playerChoice !== "ножиці" && playerChoice !== "бумага") {
    alert("Помилка! Оберітьт один з трьох варіантів: камінь, Ножницы или бумага");
    return;
  }
   if (playerChoice === choices[computerChoice]) {
    alert("50/50");
  } else if (playerChoice === "камінь" && choices[computerChoice] === "ножиці" ||
             playerChoice === "ножиці" && choices[computerChoice] === "бумага" ||
             playerChoice === "бумага" && choices[computerChoice] === "камінь") {
    alert("Ви Виграли");
  } else {
    alert("Ви програли");
  }
}
document.getElementById("play-btn");

//Завдання4
function getFiboNumber() {
            var n = +prompt("введіть кількість чисел Фібоначі: ");
            var firstValue = 1;
            var secondValue = 2;
            var nextValue = firstValue + secondValue;
            if (n < 1 || isNaN(n)) {
                document.write("Помилка");
            }
           else {

                for (var i = 1; i <= n; i++) {
                    firstValue = secondValue;
                    secondValue = nextValue;
                    nextValue = firstValue + secondValue;
                }
            }
            document.write("<br>Число Фібоначі: " + nextValue);
        }
        getFiboNumber();




